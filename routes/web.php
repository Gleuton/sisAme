<?php


Route::get('/', function () {
    return view('index');
});

Route::get('/paciente/cadastro', 'PatientController@formCad')->name('formCad');

Route::post('/paciente/cadastro', 'PatientController@create')->name('submitCad');

Route::get('/getCidade/{uf}', 'CityController@getCity');

Route::get('/login/', 'LoginController@index')->name('login');

Route::post('/login/', 'LoginController@login');

Route::get('/logout/', 'LoginController@logout');

Route::middleware(['middleware' => 'patient'])->prefix('paciente')->group(function () {

    Route::get('editar', 'PatientController@formUp');

    Route::post('user/update', 'PatientController@update');

    Route::get('home/{status?}', 'PatientHomeController@index');

    Route::get('unidades', 'MedicalAssistanceUnitController@index');

    Route::get('historico', 'RequestController@historic');

    Route::get('atendimento/{unidade}', 'RequestController@index');

    Route::post('atendimento', 'RequestController@create');

    Route::post('disable', 'RequestController@disable');

    Route::get('unidade/{id}', 'MedicalAssistanceUnitController@getUnity');

    Route::get('unidades/search/{json}', 'MedicalAssistanceUnitController@search');
});

