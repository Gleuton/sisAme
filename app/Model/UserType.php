<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserType extends Model
{
    protected $connection = 'mysql';

    protected $table = 'TB_TIPO_USUARIO';

    protected $primaryKey = 'CO_TIPO_USUARIO';

    protected $guarded = ['CO_TIPO_USUARIO'];

    public $timestamps = false;

    protected $fillable = [
        'TIPO',
        'DESCRICAO'
    ];
}
