<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Specialty extends Model
{
    protected $connection = 'mysql';

    protected $table = 'TB_ESPECIALIDADE';

    protected $primaryKey = 'CO_ESPECIALIDADE';

    protected $guarded = ['CO_ESPECIALIDADE'];

    public $timestamps = false;

    protected $fillable = [
        'ESPECIALIDADE',
        'DESCRICAO'
    ];
}
