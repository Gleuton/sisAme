<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $connection = 'mysql';

    protected $table = 'TB_ENDERECO';

    protected $primaryKey = 'CO_ENDERECO';

    protected $guarded = ['CO_ENDERECO'];

    public $timestamps = false;

    protected $fillable = [
        'ENDERECO',
        'BAIRRO',
        'CO_CIDADE',
        'LATITUDE',
        'LONGITUDE',
        'CEP'
    ];

}
