<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Telephone extends Model
{
    protected $connection = 'mysql';

    protected $table = 'TB_TELEFONE';

    protected $primaryKey = 'CO_TELEFONE';

    protected $guarded = ['CO_TELEFONE'];

    public $timestamps = false;

    protected $fillable = [
        'NU_TELEFONE',
        'CO_TIPO_TELEFONE',
        'CO_CONTATO'
    ];
}
