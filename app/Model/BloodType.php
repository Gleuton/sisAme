<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class BloodType extends Model
{
    protected $connection = 'mysql';

    protected $table = 'TB_TIPO_SANGUINEO';

    protected $primaryKey = 'CO_TIPO_SANGUINEO';

    protected $guarded = ['CO_TIPO_SANGUINEO'];

    public $timestamps = false;

    protected $fillable = [
        'TIPO',
        'DESCRICAO'
    ];
}
