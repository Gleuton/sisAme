<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class HealthProblem extends Model
{
    protected $connection = 'mysql';

    protected $table = 'TB_PROBLEMA_SAUDE';

    protected $primaryKey = 'CO_PROBLEMA_SAUDE';

    protected $guarded = ['CO_PROBLEMA_SAUDE'];

    public $timestamps = false;

    protected $fillable = [
        'PROBLEMA',
        'DESCRICAO'
    ];
}
