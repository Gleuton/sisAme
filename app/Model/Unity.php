<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Unity extends Model
{
    protected $connection = 'mysql';

    protected $table = 'TB_UNIDADE_ATENDIMENTO';

    protected $primaryKey = 'CO_UNIDADE_ATENDIMENTO';

    protected $guarded = ['CO_UNIDADE_ATENDIMENTO'];

    public $timestamps = false;

}
