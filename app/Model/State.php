<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    protected $connection = 'mysql';

    protected $table = 'TB_ESTADO';

    protected $primaryKey = 'CO_ESTADO';

    protected $guarded = ['CO_ESTADO'];

    public $timestamps = false;

    protected $fillable = [
        'NOME',
        'UF'
    ];
}
