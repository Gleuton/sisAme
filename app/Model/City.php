<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $connection = 'mysql';

    protected $table = 'TB_CIDADE';

    protected $primaryKey = 'CO_CIDADE';

    protected $guarded = ['CO_CIDADE'];

    public $timestamps = false;

    protected $fillable = [
        'NOME',
        'CO_ESTADO'
    ];
}
