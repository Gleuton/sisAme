<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SymptomsType extends Model
{
    protected $connection = 'mysql';

    protected $table = 'TB_TIPO_SINTOMA';

    protected $primaryKey = 'CO_TIPO_SINTOMA';

    protected $guarded = ['CO_TIPO_SINTOMA'];

    public $timestamps = false;
}
