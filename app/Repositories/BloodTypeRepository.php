<?php
/**
 * Project: sisAme
 * User: gleuton
 * Date: 26/05/18
 */

namespace App\Repositories;


use App\Model\BloodType;


class BloodTypeRepository
{
    private $model;

    public function __construct(BloodType $bloodType)
    {
        $this->model = $bloodType;
    }

    public function getAll()
    {
        return $this->model->all()->sortBy('TIPO');
    }

}