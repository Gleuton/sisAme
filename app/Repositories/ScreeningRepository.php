<?php
/**
 * Project: sisAme
 * User: Gleuton
 * Date: 26/05/18
 */

namespace App\Repositories;

use App\Model\Screening;


class ScreeningRepository
{
    private $model;

    public function __construct(Screening $screening)
    {
        $this->model = $screening;
    }

    public function create(array $data) :int
    {
        $this->model->CO_USUARIO = $data['usuario'];
        $this->model->DT_ATENDIMENTO = $data['data'];
        $this->model->CO_UNIDADE_ATENDIMENTO = $data['unidade'];
        $this->model->STATUS = 'Em Aprovação';

        $this->model->save();
        return $this->model->CO_PRETRIAGEM;
    }

    public function getByUser(int $coUser)
    {
        return $this->model
                    ->join('TB_UNIDADE_ATENDIMENTO',
                            'TB_PRETRIAGEM.CO_UNIDADE_ATENDIMENTO',
                            'TB_UNIDADE_ATENDIMENTO.CO_UNIDADE_ATENDIMENTO')
                    ->where('CO_USUARIO',$coUser)
                    ->where('STATUS','!=','Desabilitado')
                    ->orderBy('DT_ATENDIMENTO')
                    ->paginate(5);
    }

    public function disable(int $coSolicitacao)
    {
        $solicitacao = $this->model->find($coSolicitacao);

        $solicitacao->STATUS = 'Desabilitado';

        $solicitacao->save();

        return true;
    }
}