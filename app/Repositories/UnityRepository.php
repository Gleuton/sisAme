<?php
/**
 * Project: sisAme
 * User: gleuton
 * Date: 26/05/18
 */

namespace App\Repositories;


use App\Model\Unity;

class UnityRepository
{
    private $model;

    public function __construct(Unity $unity)
    {
        $this->model = $unity;
    }

    public function search($city, $specialty)
    {
        return $this->model
            ->join('TB_ENDERECO', 'TB_UNIDADE_ATENDIMENTO.CO_ENDERECO', 'TB_ENDERECO.CO_ENDERECO')
            ->join('RL_UNIDADE_ATENDIMENTO_ESPECIALIDADE',
                'TB_UNIDADE_ATENDIMENTO.CO_UNIDADE_ATENDIMENTO',
                'RL_UNIDADE_ATENDIMENTO_ESPECIALIDADE.CO_UNIDADE_ATENDIMENTO')
            ->join('TB_ESPECIALIDADE',
                'RL_UNIDADE_ATENDIMENTO_ESPECIALIDADE.CO_ESPECIALIDADE',
                'TB_ESPECIALIDADE.CO_ESPECIALIDADE')
            ->where('TB_ENDERECO.CO_CIDADE', $city)
            ->where('TB_ESPECIALIDADE.CO_ESPECIALIDADE', $specialty)
            ->get();
    }

    public function getUnity(int $co_unidade)
    {
        return $this->model
            ->join('TB_ENDERECO', 'TB_UNIDADE_ATENDIMENTO.CO_ENDERECO', 'TB_ENDERECO.CO_ENDERECO')
            ->join('TB_CONTATO', 'TB_UNIDADE_ATENDIMENTO.CO_CONTATO', 'TB_CONTATO.CO_CONTATO')
            ->join('TB_TELEFONE', 'TB_CONTATO.CO_CONTATO', 'TB_TELEFONE.CO_CONTATO')
            ->join('RL_UNIDADE_ATENDIMENTO_ESPECIALIDADE',
                'TB_UNIDADE_ATENDIMENTO.CO_UNIDADE_ATENDIMENTO',
                'RL_UNIDADE_ATENDIMENTO_ESPECIALIDADE.CO_UNIDADE_ATENDIMENTO')
            ->join('TB_ESPECIALIDADE',
                'RL_UNIDADE_ATENDIMENTO_ESPECIALIDADE.CO_ESPECIALIDADE',
                'TB_ESPECIALIDADE.CO_ESPECIALIDADE')
            ->where('TB_UNIDADE_ATENDIMENTO.CO_UNIDADE_ATENDIMENTO', $co_unidade)
            ->first();
    }
}