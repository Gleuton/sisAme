<?php
/**
 * Project: sisAme
 * User: gleuton
 * Date: 26/05/18
 */

namespace App\Repositories;


use App\Model\City;

class CityRepository
{
    private $model;

    public function __construct(City $city)
    {
        $this->model = $city;
    }

    public function getByUf(int $co_estado)
    {
        return $this->model->where('CO_ESTADO',$co_estado)->orderBy('NOME')->get();
    }

}