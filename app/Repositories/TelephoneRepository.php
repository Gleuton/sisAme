<?php
/**
 * Project: sisAme
 * User: gleuton
 * Date: 26/05/18
 */

namespace App\Repositories;

use App\Model\Telephone;

class TelephoneRepository
{
    private $model;

    public function __construct(Telephone $address)
    {
        $this->model = $address;
    }

    public function create(array $data)
    {
        $this->model->NU_TELEFONE = $data['telefone'];
        $this->model->CO_TIPO_TELEFONE = $data['tipoTelefone'] ?? null;
        $this->model->CO_CONTATO = $data['co_contato'];

        $this->model->save();
        return $this->model->CO_TELEFONE;
    }

    public function update(array $data,int $coTelefone)
    {

        $telefone = $this->model->find($coTelefone);

        $telefone->NU_TELEFONE = $data['telefone'];
        $telefone->CO_CONTATO = $data['co_contato'];

        $telefone->save();
        return $telefone->CO_TELEFONE;
    }

}