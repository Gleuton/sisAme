<?php
/**
 * Project: sisAme
 * User: gleuton
 * Date: 26/05/18
 */

namespace App\Repositories;

use App\Model\Contact;

class ContactRepository
{
    private $model;

    public function __construct(Contact $address)
    {
        $this->model = $address;
    }

    public function create(array $data)
    {
        $this->model->EMAIL = $data['email'];

        $this->model->save();
        return $this->model->CO_CONTATO;
    }

    public function update(array $data,int $coContato)
    {
        $contato = $this->model->find($coContato);
        $contato->EMAIL = $data['email'];

        $contato->save();

        return $contato->CO_CONTATO;
    }

}