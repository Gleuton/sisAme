<?php
/**
 * Project: sisAme
 * User: gleuton
 * Date: 26/05/18
 */

namespace App\Repositories;


use App\Model\Address;

class AddressRepository
{
    private $model;

    public function __construct(Address $address)
    {
        $this->model = $address;
    }

    public function create(array $data)
    {
        $this->model->ENDERECO = $data['endereco'];
        $this->model->CO_CIDADE = $data['cidade'];
        $this->model->BAIRRO = $data['bairro'];
        $this->model->CEP = $data['cep'];
        $this->model->save();
        return $this->model->CO_ENDERECO;
    }

    public function update(array $data, int $coEndereco)
    {
        $endereco = $this->model->find($coEndereco);
        $endereco->ENDERECO = $data['endereco'];
        $endereco->CO_CIDADE = $data['cidade'];
        $endereco->BAIRRO = $data['bairro'];
        $endereco->CEP = $data['cep'];
        $endereco->save();

        return $endereco->CO_ENDERECO;
    }

}