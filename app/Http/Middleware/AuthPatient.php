<?php

namespace App\Http\Middleware;

use App\Http\Controllers\LoginController;
use Closure;

class AuthPatient
{
    private $logInController;

    public function __construct(LoginController $logInController)
    {
        $this->logInController = $logInController;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$this->logInController->validator()) {
            return $this->logInController->redirect();
        }
        return $next($request);
    }
}
