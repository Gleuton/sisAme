<?php

namespace App\Http\Requests;


class UserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $coUser = parent::get('CO_USUARIO');
        $coContato = parent::get('CO_CONTATO');

        $contatoValidate = $coContato . ',CO_CONTATO';

        if (!empty($coUser) && !empty($coContato)) {
            $validate = [
                'nome' => 'min:3|max:100|required',
                'sobrenome' => 'min:4|max:100|required',
                'dt_nascimento' => 'date|required',
                'sexo' => 'required',
                'cep' => 'min:8|max:8|required',
                'cidade' => 'required',
                'endereco' => 'min:4|max:100|required',
                'bairro' => 'min:4|max:150|required',
                'email' => 'min: 7|max:100|required|unique:TB_CONTATO,EMAIL,' . $contatoValidate,
                'telefone' => 'required',
                'senha' => 'required|min:4|max:100',
                'confirmar_senha' => 'required|same:senha'
            ];

            if (empty(parent::get('senha'))){
                unset($validate['senha']);
                unset($validate['confirmar_senha']);
            }

            return $validate;
        }

        return [
            'nome' => 'min:3|max:100|required',
            'sobrenome' => 'min:4|max:100|required',
            'rg' => 'min:7|max:50|required|unique:TB_USUARIO',
            'cpf' => 'min:11|max:11|required|unique:TB_USUARIO',
            'dt_nascimento' => 'date|required',
            'sexo' => 'required',
            'cep' => 'min:8|max:8|required',
            'cidade' => 'required',
            'endereco' => 'min:4|max:100|required',
            'bairro' => 'min:4|max:150|required',
            'email' => 'min: 7|max:100|required|unique:TB_CONTATO',
            'telefone' => 'required',
            'senha' => 'min:4|max:100|required',
            'confirmar_senha' => 'required|same:senha'
        ];
    }
}
