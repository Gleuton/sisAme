<?php

namespace App\Http\Requests;

/**
 *
 * @author Gleuton Dutra <gleuton.dutra@gmail.com>
 */

use Illuminate\Foundation\Http\FormRequest;

abstract class Request extends FormRequest
{
    public function messages()
    {
        return [
            'required' => ':attribute é um campo é obrigatório.',
            'min' => 'O campo :attribute não deve ser menor que :min.',
            'max' => ' O campo :attribute não deve ser maior que :max.',
            'unique' =>':attribute informado já está cadastrado no sistema.', 
            'same' => 'O campo :attribute não corresponde ao campo senha'
        ];
    }

}