<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Model\State;
use App\Repositories\AddressRepository;
use App\Repositories\ContactRepository;
use App\Repositories\HealthProblemRepository;
use App\Repositories\BloodTypeRepository;
use App\Repositories\TelephoneRepository;
use App\Repositories\UserTypeRepository;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\DB;


class PatientController extends Controller
{
    private $modelState;
    private $repositoryProblem;
    private $repositoryBlood;
    private $repositoryAddress;
    private $repositoryUserType;
    private $repositoryUser;
    private $repositoryCotact;
    private $repositoryTelephone;
    private $login;

    public function __construct(State $state,
                                HealthProblemRepository $problemRepository,
                                AddressRepository $addressRepository,
                                BloodTypeRepository $bloodTypeRepository,
                                ContactRepository $contactRepository,
                                TelephoneRepository $telephoneRepository,
                                UserTypeRepository $userTypeRepository,
                                LoginController $loginController,
                                UserRepository $userRepository)
    {
        $this->modelState = $state;
        $this->repositoryProblem = $problemRepository;
        $this->repositoryBlood = $bloodTypeRepository;
        $this->repositoryAddress = $addressRepository;
        $this->repositoryCotact = $contactRepository;
        $this->repositoryTelephone = $telephoneRepository;
        $this->repositoryUserType = $userTypeRepository;
        $this->repositoryUser = $userRepository;
        $this->login = $loginController;
    }

    public function formUp()
    {
        $user = $this->repositoryUser->getUser(
            $this->login->getUser()['id']
        );
        $action = 'PatientController@update';
        $estados = $this->modelState->all()->sortBy('UF');
        $problemasSaude = $this->repositoryProblem->getAll();
        $tipoSanguineo = $this->repositoryBlood->getAll();

        $user->ST_NASCIMENTO = date('Y-m-d', strtotime($user->DT_NASCIMENTO));

        return view('patient.form')
            ->with(compact('estados'))
            ->with(compact('tipoSanguineo'))
            ->with(compact('user'))
            ->with(compact('action'))
            ->with(compact('problemasSaude'));
    }

    public function formCad()
    {
        $action = 'PatientController@create';
        $estados = $this->modelState->all()->sortBy('UF');
        $problemasSaude = $this->repositoryProblem->getAll();
        $tipoSanguineo = $this->repositoryBlood->getAll();

        return view('patient.form')
            ->with(compact('estados'))
            ->with(compact('tipoSanguineo'))
            ->with(compact('action'))
            ->with(compact('user'))
            ->with(compact('problemasSaude'));
    }

    public function create(UserRequest $request)
    {
        $data = $request->all();

        DB::beginTransaction();

        try {
            $data['co_endereco'] = $this->repositoryAddress->create($data);
            $data['co_tipo_usuario'] = $this->repositoryUserType->getByType('Paciente')->CO_TIPO_USUARIO;
            $data['co_contato'] = $this->repositoryCotact->create($data);
            $this->repositoryTelephone->create($data);
            $this->repositoryUser->create($data);

            DB::commit();

            return redirect('login');
        } catch (\Exception $e) {
            DB::rollback();
        }
        return false;
    }

    public function update(UserRequest $request)
    {
        $data = $request->all();
        DB::beginTransaction();

        try {
            $user = $this->repositoryUser->getUser(
                $data['CO_USUARIO']
            );

            $this->repositoryUser->update($data, $data['CO_USUARIO']);

            $data['co_contato'] = $this->repositoryCotact->update($data, $user->CO_CONTATO);

            $this->repositoryTelephone->update($data, $user->CO_TELEFONE);
            $this->repositoryAddress->update($data,$user->CO_ENDERECO);

            DB::commit();

            return redirect('paciente/home');
        } catch (\Exception $e) {
            DB::rollback();
        }
        return false;
    }
}
