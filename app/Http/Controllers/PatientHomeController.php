<?php

namespace App\Http\Controllers;


class PatientHomeController extends Controller
{


    public function __construct()
    {

    }

    public function index($status = null)
    {
        return view('patient.home')->with(['status'=>$status]);
    }

}
