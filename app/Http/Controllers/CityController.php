<?php

namespace App\Http\Controllers;


use App\Repositories\CityRepository;



class CityController extends Controller
{
    private $modelCity;
    public function __construct(CityRepository $city)
    {
        $this->modelCity = $city;
    }

    public function getCity(int $uf)
    {
        return $this->modelCity->getByUf($uf);
    }

}
