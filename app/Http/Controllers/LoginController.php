<?php
/**
 * Project: sisAme
 * User: gleuton
 * Date: 27/05/18
 */

namespace App\Http\Controllers;


use App\Http\Requests\LoginRequest;
use App\Repositories\UserRepository;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;


class LoginController extends Controller
{
    private $repositoryUser;
    private $request;

    public function __construct(UserRepository $userRepository, Request $request)
    {
        $this->repositoryUser = $userRepository;
        $this->request = $request;
    }

    public function index()
    {
        return view('patient.login');
    }

    public function login(LoginRequest $request)
    {
        $email = $request->get('email');
        $password = $request->get('senha');

        $user = $this->repositoryUser->getByEmail($email);

        if (!empty($user) && password_verify($password, $user->SENHA)) {
            $this->createSession($user);

            return Redirect::to('/paciente/home');
        };

        $erro = 'Credenciais Incorretas';

        return view('patient.login')->with('erro', $erro);

    }

    public function logout()
    {
        $this->request->getSession()->forget('login');
        return redirect('paciente\home');
    }

    public function validator(): bool
    {
        $session = $this->request->getSession()->get('login');

        if (!isset($session) || !$session[0]['validation']) {
            return false;
        }
        return true;
    }

    public function getUser()
    {
        return $this->request->getSession()->get('login')[0];
    }

    public function redirect()
    {
        return redirect('login');
    }

    private function createSession($data): void
    {
        $dataUser['id'] = $data['CO_USUARIO'];
        $dataUser['validation'] = true;
        $dataUser['name'] = $data['NOME'];
        $dataUser['lastName'] = $data['SOBRENOME'];
        $dataUser['type'] = $data['TIPO'];

        $this->request->getSession()->push('login', $dataUser);
    }
}