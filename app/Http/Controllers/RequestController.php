<?php
/**
 * Project: sisAme
 * User: gleuton
 * Date: 27/05/18
 */

namespace App\Http\Controllers;


use App\Model\SymptomsType;
use App\Repositories\ScreeningRepository;
use App\Repositories\SymptomRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RequestController extends Controller
{

    private $symptomsTypeModel;
    private $loginController;
    private $screeningRepository;
    private $symptomRepository;

    public function __construct(
        SymptomsType $symptomsType,
        LoginController $loginController,
        ScreeningRepository $screeningRepository,
        SymptomRepository $symptomRepository
    )
    {
        $this->symptomsTypeModel = $symptomsType;
        $this->loginController = $loginController;
        $this->screeningRepository = $screeningRepository;
        $this->symptomRepository = $symptomRepository;
    }

    public function index(int $unidade): string
    {
        $sintomas = $this->symptomsTypeModel->all()->sortBy('SINTOMA');

        return view('patient.request')
            ->with(compact('unidade'))
            ->with(compact('sintomas'));
    }

    public function create(Request $request)
    {
        $status = 'false';
        $data = $request->all();

        $sintomas = [];
        foreach ($data['sintomas'] as $k => $sintoma) {
            $sintomas[$k]['sintoma'] = $sintoma;
            $sintomas[$k]['intensidade'] = $data['intensidades'][$k];
        }

        DB::beginTransaction();
        try {
            $triagem['unidade'] = $data['unidade'];

            $triagem['usuario'] = $this->loginController->getUser()['id'];
            $triagem['data'] = date("Y-m-d");

            $co_triagem = $this->screeningRepository->create($triagem);

            foreach ($sintomas as $sintoma) {

                $this->symptomRepository->create($co_triagem, $sintoma);
            }

            DB::commit();
            $status = 'true';
        } catch (\Exception $e) {
            DB::rollback();
        }
        return redirect('paciente/home/' . $status);
    }

    public function historic()
    {
        $user = $this->loginController->getUser();
        $historic = $this->screeningRepository->getByUser($user['id']);
        foreach ($historic as $value){


            $value->DATA_ATENDIMENTO = date_format ( $value->DT_ATENDIMENTO , 'd/m/Y' );
        }
        return view('patient.historic')
            ->with(compact('historic'));
    }

    public function disable(Request $request)
    {
        $data = $request->all();
        $this->screeningRepository->disable($data['coSolicitacao']);
        return redirect('paciente/historico');
    }
}