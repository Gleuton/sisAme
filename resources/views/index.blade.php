@component('patient.layouts.default')
    <section class="hero" data-bg-img="{{ asset('img/parallax-bg.jpg')}}"
             data-settings='{"stellar-background-ratio": 0.6}'
             data-toggle="parallax-bg">
        <div class="container text-center">
            <div class="row">
                <div class="col-md-12">
                    <a class="hero-brand" href="/" title="Home"><img alt="SisAme Logo"
                                                                     src="{{ asset('img/logo_grande.png')}}"></a>
                </div>
            </div>
            <div class="col-md-12">
                <h1>
                    SisAme
                </h1>

                <p class="tagline">
                    Localize os hospitais e clínicas mais próximos de você!
                </p>
                <a class="btn btn-full btn-about" href="#about">Saiba Mais!</a>
            </div>
        </div>

    </section>

    <section class="about" id="about">
        <div class="container text-center">
            <h1>
                Sobre SisAme
            </h1>

            <h2>
                O SisAme, Sistema de Atendimento Médico Emergencial,
                é uma webApp desenvolvida por alunos da UDF para facilitar
                a localização de hospitais e clínicas, tem como objetivo
                também otimizar o processo de triagem em atendimentos emergenciais.
            </h2>

        </div>
    </section>
    <!-- /About -->
    <!-- convite para Cadastrar -->

    <section class="cta">
        <div class="container">
            <div class="row">
                <div class="col-lg-9 col-sm-12 text-lg-left text-center">
                    <h2>
                        Cadastre-se
                    </h2>

                    <p>
                        Cadastre-se no SisAme e utilize nossos serviços!
                    </p>
                </div>

                <div class="col-lg-3 col-sm-12 text-lg-right text-center">
                    <a class="btn btn-ghost" href="/paciente/cadastro">Cadastrar</a>
                </div>
            </div>
        </div>
    </section>
@endcomponent