@component('patient.layouts.inside')
    @slot('title') Histórico @endslot

    <section>
        <div class="container">
            @if(count($historic) > 0)
            <div class="table-responsive">
                <table class="table tb-list">
                    <thead>
                    <tr>
                        <th>Unidade de Atendimento</th>
                        <th>Data da solicitação</th>
                        <th>Status</th>
                        <th class="text-center">Ações</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($historic as $value)
                        <tr>
                            <td>{{$value->NOME_FANTAZIA}}</td>
                            <td>{{$value->DATA_ATENDIMENTO}}</td>
                            <td>{{$value->STATUS}}</td>
                            <td class="text-center">
                                <button type="button"
                                        class="cancelar"
                                        value="{{$value->CO_PRETRIAGEM}}"
                                        data-toggle="modal"
                                        data-target="#modal_cancelar">Cancelar</button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{$historic->links()}}
            </div>
            @else
                <div class="alert alert-warning" role="alert">
                    Não cosnstam dados em seu histórico de atendimentos!
                </div>
            @endif
        </div>
    </section>

    <!-- Modal -->
    <div class="modal fade" id="modal_cancelar" tabindex="-1" role="dialog" aria-labelledby="tile_cancelar" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="tile_cancelar">Realmente deja cancelar esta solicitação?</h5>
                </div>
                <form action="{{action('RequestController@disable')}}" class="modal-footer" method="post">
                    @csrf
                    <input type="hidden" id="coSolicitacao" name="coSolicitacao" value="">
                    <button type="button" class="cancelar" data-dismiss="modal">Não</button>
                    <button type="submit" class="success">Sim</button>
                </form>
            </div>
        </div>
    </div>
@endcomponent
<script type="text/javascript">
    let cancelar = $('.cancelar');

    cancelar.on('click',function () {
        $('#coSolicitacao').val(cancelar.val());
    })
</script>