@component('patient.layouts.inside')
    @slot('title') Unidade de Atendimento @endslot

    <style>
        #map {
            height: 600px;
            width: 80%;
            margin: 0 auto;
            margin-bottom: 30px;
            border-radius: 10px;
        }

    </style>
    <div hidden="">
        <input type="hidden" id="lat" value="{{$unidade->LATITUDE}}">
        <input type="hidden" id="lng" value="{{$unidade->LONGITUDE}}">
    </div>

    <section class="cadastro" id="detalhe">
        <div class="container">
            <h3>{{$unidade->NOME_FANTAZIA}}</h3>
            <hr>
            <div class="row">
                <div class="col-md-6 com-sm-12">
                    <p><strong>Endereço:</strong> {{$unidade->ENDERECO}}</p>
                </div>
                <div class="col-md-6 com-sm-12">
                    <p><strong>Bairro:</strong> {{$unidade->BAIRRO}}</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 com-sm-12">
                    <p><strong>Telefone:</strong> {{$unidade->NU_TELEFONE}}</p>
                </div>
                <div class="col-md-6 com-sm-12">
                    <p><strong>E-Mail:</strong> {{$unidade->EMAIL}}</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 col-sm-3">
                    <a href="/paciente/unidades/" class="btn btn-form form-control">Voltar</a>
                </div>  
                <div class="col-md-6 com-sm-3"></div>
                <div class="col-md-3 com-sm-3 text-right">
                    <a href="/paciente/atendimento/{{$unidade->CO_UNIDADE_ATENDIMENTO}}"
                       class="btn btn-form form-control">Solicitar Atendimento</a>
                </div>
            </div>
        </div>

    </section>
    <div id="map"></div>
    <script src="{{asset('/js/maps.js')}}"></script>
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCLmUqwzcYP2yN1xFuVwo2NIdI6Nvf0R_k&callback=initMap">
    </script>
@endcomponent

