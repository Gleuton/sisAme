@component('patient.layouts.inside')
    @slot('title') Unidades de Atendimento @endslot
    <section class="cadastro" id="cadastro">
        <div class="container">
            <h3>Buscar Unidades de Antendimento:</h3>
            <form class="form-inline form-search" id="formSearch">
                <div class="row">
                    <div class="col-md-1 com-sm-12">
                        <select class="form-control uf" id="uf" name="estado">
                            @foreach($estados as $estado)
                                <option value="{{$estado->CO_ESTADO}}">{{$estado->UF}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-4 com-sm-12">
                        <select class="form-control cidade" id="cidade" name="cidade" required>
                        </select>
                    </div>
                    <div class="col-md-5 com-sm-12">
                        <select class="form-control espcialidade" id="espcialidade" name="espcialidade">
                            @foreach($especialidades as $especialidade)
                                <option value="{{$especialidade->CO_ESPECIALIDADE}}">{{$especialidade->ESPECIALIDADE}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-2 com-sm-12">
                        <button class='btn btn-search btn-block' id="btnSearch">
                            <span class="material-icons">search</span>
                        </button>
                    </div>
                </div>
            </form>
            <div id="listUnidades"></div>
            <div class="row">
                <div class="col-md-2 col-sm-3">
                    <a href="/paciente/home/" class="btn btn-form form-control">Voltar</a>
                </div>      
            </div>    
        </div>     
    </section>

@endcomponent
<script type="text/javascript" src="{{ asset('js/city/selectCity.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/unit/listUnits.js')}}"></script>
<script type="text/javascript">
    let uf = $('#uf');
    let city = $('#cidade');
    let btnSearch = $('#btnSearch');
    let formSearch = $('#formSearch');
    let listUnit = $('#listUnidades');

    selectCity(city, uf.val());
    uf.change(() => {
        selectCity(city, uf.val())
    });

    btnSearch.on('click',(event)=>{
        event.preventDefault();
        listUnits(
            formSearch.serializeArray(),
            listUnit
        );
    });

</script>