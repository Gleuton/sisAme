@component('patient.layouts.inside')
    @slot('title')Solicitação de antendimento @endslot
    <section class="cadastro" id="cadastro">
        <div class="container text-center">
            <h1>Solicitação de Atendimento</h1>
            <div class="row">
                <div class="col-md-2">
                </div>
                <div class="col-md-8">
                    <form action="{{action('RequestController@create')}}" method="post">
                        @csrf
                        <input type="hidden" name="unidade" value="{{$unidade}}">
                        <div class="form-group form-adm text-center">
                            <div class="row sitoma-row">
                                <div class="col-md-6 com-sm-12">
                                    <label for="sintoma">Sintomas:</label>
                                    <select class="form-control" id="sintoma" name="sintomas[]">
                                        @foreach($sintomas as $sintoma)
                                            <option value="{{$sintoma->CO_TIPO_SINTOMA}}">{{$sintoma->SINTOMA}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-4 com-sm-12">
                                    <label>Grau de intensidade:</label>
                                    <input type="number" min="1" max="10" class="form-control"
                                           name='intensidades[]' id="grau" placeholder="1-10" required>
                                </div>
                                <div class="col-md-2 com-sm-12 text-right">
                                    <div class="btn-toolbar" role="toolbar">
                                        <div class="btn-group">
                                            <button type="button" class='btn-control add btn-esquerda'>
                                                <span class="material-icons">add</span>
                                            </button>
                                            <button type="button" class='btn-control remove btn-direita' disabled>
                                                <span class="material-icons">remove</span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-5 com-sm-6 text-center">
                                     <a href="/paciente/unidade/{{$unidade}}" class="btn btn-logar ">Voltar</a>
                                </div>
                                <div class="col-md-5 com-sm-6 text-right">
                                    <button type="submit" id="enviar-solicitacao"
                                            class="btn btn-logar">Enviar
                                    </button>
                                </div>
                            </div>         
                        </div>
                    </form>

                </div>
                <div class="col-md-2">
                </div>
            </div>
        </div>
    </section>
@endcomponent
<script type="text/javascript">
    $('.add').on('click', function (e) {
        e.defaultPrevented;
        let f = $(this).parent().parent().parent().parent(),
            c = f.clone(true, true);
        c.children().find('.remove').prop("disabled", false);
        c.insertAfter(f);
    });
    $('.remove').on('click', function (e) {
        e.defaultPrevented;
        $(this).parent().parent().parent().parent().remove();
    });
</script>