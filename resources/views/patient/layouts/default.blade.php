<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>{{ $title ?? 'SISAME' }}</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script type="text/javascript" src="{{ asset('/js/app.js')}}"></script>
    <!-- Favicon -->
    <link href="{{ asset('/img/logo_branca.png')}}" rel="icon">

    <!-- Bootstrap CSS File -->
    <link rel="stylesheet" href="{{ asset('/css/app.css')}}" type="text/css">

    <!-- Libraries CSS Files -->
    <link rel="stylesheet" href="{{asset('/css/font-awesome.css')}}" type="text/css">

    <!-- Main Stylesheet File -->
    <link rel="stylesheet" href="{{ asset('/css/style.css')}}" type="text/css">
</head>

<body>

<header id="header">
    <div class="container">

        <div id="logo" class="pull-left">
            <a href="/"><img src="{{ asset('img/logo_branca.png')}}" alt="" title=""/></a>
        </div>
        <nav id="nav-menu-container">
            <ul class="nav-menu">
                <li><a href="/">Início</a></li>
                <li><a href="/paciente/cadastro/">Cadastrar</a></li>
                <li><a href="/login/">Login</a></li>
            </ul>
        </nav>
    </div>
</header>
<main id="content">

    {{$slot}}

</main>
<footer class="site-footer">
    <div class="bottom">
        <div class="container">
            <div class="row">

                <div class="col-lg-6 col-xs-12 text-lg-left text-center">
                    <p class="copyright-text">
                        © SisAme | {{date('Y')}} | Todos os Direitos Reservados
                    </p>
                </div>

                <div class="col-lg-6 col-xs-12 text-lg-right text-center">
                    <ul class="list-inline">
                        <li class="list-inline-item">
                            <a href="/">Início</a>
                        </li>

                        <li class="list-inline-item">
                            <a href="/login/">Login</a>
                        </li>

                        <li class="list-inline-item">
                            <a href="/paciente/cadastro/">Cadastrar</a>
                        </li>
                    </ul>
                </div>

            </div>
        </div>
    </div>
</footer>
</body>

</html>