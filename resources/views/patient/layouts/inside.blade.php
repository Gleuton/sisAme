<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="utf-8">
    <title>{{ $title ?? 'SISAME' }}</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script type="text/javascript" src="{{ asset('/js/app.js')}}"></script>
    <!-- Favicon -->
    <link href="{{ asset('/img/logo_branca.png')}}" rel="icon">

    <!-- Bootstrap CSS File -->
    <link rel="stylesheet" href="{{ asset('/css/app.css')}}" type="text/css">

    <!-- Libraries CSS Files -->
    <link rel="stylesheet" href="{{asset('/css/font.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('/css/font-awesome.css')}}" type="text/css">

    <link rel="stylesheet" href="{{ asset('/css/style.css')}}" type="text/css">
    <style>{{ $style ?? '' }}</style>
</head>

<body>


<header id="header">
    <div class="container">
        <div id="logo" class="pull-left">
            <a href="/paciente/home/"><img src="{{ asset('img/logo_branca.png')}}" alt="" title=""/></a>
        </div>
        <nav class="nav social-nav pull-right d-none d-lg-inline">
            <a id="logout" href="/logout/"> Sair</a>
        </nav>
    </div>
</header>


{{$slot}}


</body>

</html>