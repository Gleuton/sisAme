@component('patient.layouts.inside')
    @slot('title') SisAme @endslot
    <section class="homePaciente" id="homePaciente">

        <div class="container">
            @if(isset($status))
                @if($status==='true')
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        Solicitação de atendimento efetuada com sucesso!
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @else
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <strong>Problema ao realizar solicitação!</strong>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif
            @endif
            <div class="row">
                <div class="feature-col col-lg-4 col-xs-12">
                    <a class="card card-block text-center" href="/paciente/editar/">
                        <div>
                            <div class="feature-icon">
                                <i class="material-icons">assignment_ind</i>
                            </div>
                        </div>

                        <div>
                            <h3> Meus Dados </h3>

                            <p>
                                Dados Pessoais e Informações de login.
                            </p>
                        </div>
                    </a>
                </div>

                <div class="feature-col col-lg-4 col-xs-12">
                    <div class="card text-center">
                        <div>
                            <div class="feature-icon">
                                <i class="material-icons">local_hospital</i>
                            </div>
                        </div>

                        <div>
                            <h3>Lista de Hospitais e Clínicas</h3>
                            <p>Lista de hospitais registrados e informações gerais sobre eles.</p>
                        </div>
                    </div>
                </div>

                <div class="feature-col col-lg-4 col-xs-12">
                    <div class="card card-block text-center">
                        <div>
                            <div class="feature-icon">
                                <i class="material-icons">location_on</i>
                            </div>
                        </div>
                        <div>
                            <h3>Localizar Hospitais e Clínicas</h3>
                            <p>Localize no mapa hospitais e clínicas mais próximos de você. </p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">

                <div class="feature-col col-lg-4 col-xs-12">
                    <a class="card card-block text-center" href="/paciente/unidades/">
                        <div>
                            <div class="feature-icon">
                                <i class="material-icons"> note_add</i>
                            </div>
                        </div>

                        <div>
                            <h3>Nova Solicitação de Atendimento</h3>
                            <p>Solicitar novo atendimento.</p>
                        </div>
                    </a>
                </div>

                <div class="feature-col col-lg-4 col-xs-12">
                    <a class="card card-block text-center" href="/paciente/historico/">
                        <div>
                            <div class="feature-icon">
                                <i class="material-icons">history</i>
                            </div>
                        </div>

                        <div>
                            <h3>Histórico</h3>
                            <p>Histórico de Atividades.</p>
                        </div>
                    </a>
                </div>

                <div class="feature-col col-lg-4 col-xs-12">
                    <div class="card card-block text-center">
                        <div>
                            <div class="feature-icon">
                                <i class="material-icons">forum</i>
                            </div>
                        </div>

                        <div>
                            <h3>Fale Conosco</h3>
                            <p>Tire suas dúvidas!</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endcomponent