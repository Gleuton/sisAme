function listUnits(formData, destiny) {
    let url = '/paciente/unidades/search/';
    let data = JSON.stringify(formData);

    ajax(url, data, destiny);

    function ajax(url, data, destiny) {
        destiny.html('');
        $.get(
            url + data,
            data => destiny.html(data.trim())
        );
    }

}