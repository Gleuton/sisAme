let lat = parseFloat($('#lat').val());
let lng = parseFloat($('#lng').val());


function initMap() {
    let uluru = {lat: lat, lng: lng};
    let googleM = google.maps;
    let map = new googleM.Map(document.getElementById('map'), {
        zoom: 13,
        center: uluru
    });
    let unidade = new googleM.Marker({
        position: uluru,
        map: map,
        title: 'unidade'

    });
    navigator.geolocation.getCurrentPosition(locationSuccess);

    function locationSuccess(position) {
        let latitude = position.coords.latitude;
        let longitude = position.coords.longitude;
        let usuario = new googleM.Marker({
            position: {lat: latitude, lng: longitude},
            map: map,
            draggable: true,
            title: 'you'
        });
    }
}



