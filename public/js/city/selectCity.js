function selectCity(elemet, uf, coCity) {

    elemet.html('<option value="">Selecione a Cidade</option>');

    if (uf) {
        ajax();
    }

    function selectBilder(itens) {
        itens.forEach((item) => {
            elemet.append($('<option>', {
                value: item.CO_CIDADE,
                text: item.NOME
            }));
            elemet.val(coCity);
        });
    }

    function ajax() {
        $.get(
            "/getCidade/" + uf,
            data => selectBilder(data)
        );
    }

}